<?php

//[SECTION] Objects as variables

$buildingObj = (object)[
	'name' =>'Caswyn Building',
	'floors' => 8,
	'address' => (object) [
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];

//[SECTION] Object from Classes

class Building {
	public $name;
	public $floors;
	public $address;

	//constructor
	//A constructor is used during the creation of an object to provide the initial values of each property

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//methods
	public function printName() {
		return "The name of the building is $this->name";
	}
}

$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$building2 = new Building('Manulife Building', 51, 'Commonwealth Avenue, Quezon City, Philippines');


//[SECTION] Inheritance and Polymorphism

class Condominium extends Building {
	//$name, $floors, and $addressare inherited from the Building class to this class
	//It means that condominiums also have a name,floors, address property, as well as the printName() method

	public function printName() {
		return "The name of this condominium is $this->name";
	}
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');